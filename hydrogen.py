import numpy as np

def density(r, Z):
    # todo: other methods
    """
    :param r: radius
    :return: electron density of hydrogen at r from origin
    """
    return np.exp(-Z/2*r)*np.square(np.square(Z))/(64*np.pi)

def Hu(r, Z):
    return r*np.exp(-Z*r)

def NoOP(Z): # todo improve later
    assert Z <= 36, "Only support up to Kr, max el number = 36" # only support up to Kr no-el = 36
    if 1 <= Z <= 2:
        return 1
    elif 2 < Z <= 10:
        return 2
    elif 8 < Z <= 18:
        return 3
    elif 18 < Z <= 36:
        return 4
    else:
        return None

