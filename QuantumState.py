"""
Class that stores a quantum state, with numbers l, n and Energy
"""
class QuantumState:
    def __init__(self, n, l, E=0):
        self.n = n # int
        self.l = l # int
        self.E = E # Energy
        self.deg = 0


    def __lt__(self, other): # Less than override
        return self.E < other.E

    def __gt__(self, other):
        return  self.E > other.E

    def __eq__(self, other):
        return self.E == other.E

    def __repr__(self):
        string = "Quantum state of Atom, n = " + str(self.n) + " , l = " + str(self.l) + "."
        return string

    def setDensity(self, rho_nl):
        self.rho_nl = rho_nl

    def setWavefunc(self, wave_nl):
        self.wave_nl = wave_nl

    def setDegeneracy(self, deg):
        self.deg = deg  # degeneracy, not how many can fit but how many el are in!

"""
Constructs all states for up to principal q. number n_principal
"""
def ConstructStates(n_principal, E=1000):
    states = []
    for n in range(0, n_principal):
        for l in range(0, n + 1):
            state = QuantumState(n, l, E)
            states.append(state)
    return states
