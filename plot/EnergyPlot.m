clear all
close all

alfa = 27.211386245988;
suffix = ".txt";

atoms = ["He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar"];
Groundenergies = [79.0051538, 203.4861694, 399.14864, 670.9809, 1030.1084, 1486.058, 2043.8428, 2715.890, 3511.696, 4420.00, 5451.06, 6604.95, 7888.53, 9305.8, 10859.7, 12556.4, 14400.8];
Ionenergies = [24.58738880, 5.39171495, 9.322699, 8.298019, 11.2602880, 14.53413, 13.618055, 17.42282, 21.564540, 5.1390769, 7.646236, 5.985769, 8.15168, 10.486686, 10.36001, 12.967632, 15.7596117]; %eV

dftE = ones(17, 1);
dftI = ones(17,1);

run("data/He_energies.m")
dftE(1) = Etot;
prefix = "He";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(1) = Ei(end); % last occupied orbital 1s

run("data/Li_energies.m")
dftE(2) = Etot;
prefix = "Li";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(2) = Ei(end-1); % last occupied orbital 2s

run("data/Be_energies.m")
dftE(3) = Etot;
prefix = "Be";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(3) = Ei(end-1); % last occupied orbital 1s

run("data/B_energies.m")
dftE(4) = Etot;
prefix = "B";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(4) = Ei(end); % last occupied orbital 2p

run("data/C_energies.m")
dftE(5) = Etot;
prefix = "C";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(5) = Ei(end); % last occupied orbital 2p

run("data/N_energies.m")
dftE(6) = Etot;
prefix = "N";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(6) = Ei(end); % last occupied orbital 2p

run("data/O_energies.m")
dftE(7) = Etot;
prefix = "O";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(7) = Ei(end); % last occupied orbital 2p

run("data/F_energies.m")
dftE(8) = Etot;
prefix = "F";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(8) = Ei(end); % last occupied orbital 2p

run("data/Ne_energies.m")
dftE(9) = Etot;
prefix = "Ne";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(9) = Ei(end); % last occupied orbital 2p

run("data/Na_energies.m")
dftE(10) = Etot;
prefix = "Na";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(10) = Ei(end-2); % last occupied orbital 3s

run("data/Mg_energies.m")
dftE(11) = Etot;
prefix = "Mg";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(11) = Ei(end-2);

run("data/Al_energies.m")
dftE(12) = Etot;
prefix = "Al";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(12) = Ei(end-1);

run("data/Si_energies.m")
dftE(13) = Etot;
prefix = "Si";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(13) = Ei(end-1);

run("data/P_energies.m")
dftE(14) = Etot;
prefix = "P";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(14) = Ei(end-1);

run("data/S_energies.m")
dftE(15) = Etot;
prefix = "S";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(15) = Ei(end-1);

run("data/Cl_energies.m")
dftE(16) = Etot;
prefix = "Cl";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(16) = Ei(end-1);

run("data/Ar_energies.m")
dftE(17) = Etot;
prefix = "Ar";
Ei = load("data/" + prefix + "_energies" + suffix);
dftI(17) = Ei(end-1);

% Groundstate energy
dftE  = -dftE*alfa;
f1 = setfig('a1', [720*3/2 720])
plot(1:17, dftE, 'ko', 'DisplayName', 'LDA', 'MarkerFaceColor', 'k')
plot(1:17, Groundenergies, 'ks', 'DisplayName', 'Measurement')
xlabel("")
ylabel("$E_{0}$ [eV]")
legend
grid on
hold on
set(gca,'XTick', 1:17, 'XTickLabel', {"He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar"})
% set(gca, 'YScale', 'log');
exportf(f1, 'plots/Energies.png')

errs = abs(dftE - Groundenergies')./dftE;
f2 = setfig('a2', [720*3/2 720])
plot(1:17, errs, 'ks', 'MarkerFaceColor', 'k')
xlabel("")
ylabel("$\delta E_{0}$")
% legend
grid on
hold on
set(gca,'XTick', 1:17, 'XTickLabel', {"He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar"})
exportf(f2, 'plots/EnergyErrors.png')

% Ionisation energy
dftI  = -dftI*alfa;
f3 = setfig('a3', [720*3/2 720])
plot(1:17, dftI, 'ko-', 'DisplayName', 'LDA', 'MarkerFaceColor', 'k')
plot(1:17, Ionenergies, 'ks-', 'DisplayName', 'Measurement')
xlabel("")
ylabel("$E_{ion}$ [eV]")
legend
grid on
hold on
set(gca,'XTick', 1:17, 'XTickLabel', {"He", "Li", "Be", "B", "C", "N", "O", "F", "Ne", "Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar"})
exportf(f3, 'plots/IonisationEnergies.png')
