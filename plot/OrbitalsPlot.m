clear all;
close all;

alfa = 27.211386245988;

% run("data/He_energies.m")
% run("data/Kr_energies.m")
run("data/Ar_energies.m")
% run("data/Ne_energies.m")
prefix = "Ar";
suffix = ".txt";

% load data
R = load("data/" + prefix + "_R" + suffix);

Vh = load("data/" + prefix + "_Vh" + suffix);
Vext = load("data/" + prefix + "_Vext" + suffix);
Ve = load("data/" + prefix + "_Ve" + suffix);
Vc = load("data/" + prefix + "_Vc" + suffix);
Vks = load("data/" + prefix + "_Vks" + suffix);

% states
states = load("data/" + prefix + "_states" + suffix);
wave = load("data/" + prefix + "_wave" + suffix);
rho = load("data/" + prefix + "_rho" + suffix);
deg = load("data/" + prefix + "_deg" + suffix);

f1 = setfig('b1', [1500 1000]);
subplot(2,2,1)
plot(R(2:end), wave(1,2:end), 'k-', 'DisplayName', "$u_{00}$")
ymax = 2;
xmax = 5;
[M,I] = max(wave(1,2:end)');
text(0.007 - xmax/1000, M + ymax/100, "$\varepsilon_{00} = -1.14 \cdot 10^2$")
legend
% ylim([-5, 2])
ylabel("$u(r)$")
xlabel("$r$")
grid on
set(gca, 'XScale', 'log');

% exportf(f1, 'plots/Ar_00.png')

% f2 = setfig('b2', [640 640]);
subplot(2,2,2)
plot(R(2:end), wave(2,2:end), 'k-', 'DisplayName', "$u_{10}$")
[M,I] = max(wave(2,2:end)');
text(0.007 - xmax/1000, M + ymax/100, "$\varepsilon_{10} = -1.08 \cdot 10^1$")
legend
ylabel("u(r)")
xlabel("$r$")
grid on
set(gca, 'XScale', 'log');
% exportf(f2, 'plots/Ar_10.png')

% f3 = setfig('b3', [640 640]);
subplot(2,2,3)
plot(R(2:end), wave(3,2:end), 'k-', 'DisplayName', "$u_{11}$")

[M,I] = max(wave(3,2:end)');
text(0.007 - xmax/1000, M + ymax/100, "$\varepsilon_{11} = -8.44$")
legend
ylabel("$u(r)$")
xlabel("$r$")
grid on
set(gca, 'XScale', 'log');
% exportf(f3, 'plots/Ar_11.png')

% f4 = setfig('b4', [640 640]);
subplot(2,2,4)
plot(R(2:end), wave(4,2:end), 'k-', 'DisplayName', "$u_{21}$")

[M,I] = max(wave(4,2:end)');
text(0.007 - xmax/1000, M - 0.09*M, "$\varepsilon_{21} = -0.883$")
legend
ylabel("$u(r)$")
xlabel("$r$")
grid on
set(gca, 'XScale', 'log');
% exportf(f4, 'plots/Ar_21.png')

exportf(f1, 'plots/Ar_4states.png')