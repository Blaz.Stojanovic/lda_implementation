clear all;
close all;

run("data/Kr_energies.m")
% run("data/Ar_energies.m")
% run("data/Ne_energies.m")
% prefix = "data/Kr";
prefix = "Kr";
suffix = ".txt";

% load data
R = load("data/" + prefix + "_R" + suffix);

Vh = load("data/" + prefix + "_Vh" + suffix);
Vext = load("data/" + prefix + "_Vext" + suffix);
Ve = load("data/" + prefix + "_Ve" + suffix);
Vc = load("data/" + prefix + "_Vc" + suffix);
Vks = load("data/" + prefix + "_Vks" + suffix);

% states
states = load("data/" + prefix + "_states" + suffix);
rho = load("data/" + prefix + "_rho" + suffix);
deg = load("data/" + prefix + "_deg" + suffix);

% plot potentials
f1 = setfig('a1', [2*640 640]);
title("Hartree potential of self consistent solution of Neon")
plot(R(2:end), Vh(2:end), 'k--', 'DisplayName', "Hartree potential $V_h(r)$" )
plot(R(2:end), R(2:end).*Vh(2:end), '--', 'DisplayName', "$U_h(r)$" )
legend
xlim([0.0, 10])
% ylim([-5, 2])
ylabel("$V($a.u$)$")
xlabel("$r$")
% set(gca, 'YScale', 'log');
set(gca, 'XScale', 'log');
exportf(f1, 'plots/Ne_hart.png')

% % ext
% f2 = setfig('a2', [720 720]);
% % title("Potentials, " + prefix)
% plot(R(2:end), Vext(2:end), 'k--', 'DisplayName', "External" )
% legend
% xlim([0.0, 10])
% % ylim([-5, 2])
% ylabel("$V($a.u$)$")
% xlabel("$r$")
% % set(gca, 'YScale', 'log');
% set(gca, 'XScale', 'log');
% exportf(f2, 'plots/Ne_ext.png')
% 
% % ex
% f3 = setfig('a3', [720 720]);
% % title("Potentials, " + prefix)
% plot(R(2:end), Ve(2:end), 'k--', 'DisplayName', "Exchange" )
% legend
% xlim([0.0, 10])
% % ylim([-5, 2])
% ylabel("$V($a.u$)$")
% xlabel("$r$")
% % set(gca, 'YScale', 'log');
% set(gca, 'XScale', 'log');
% exportf(f3, 'plots/Ne_exc.png')
% 
% % cor
% f4 = setfig('a4', [720 720]);
% % title("Potentials, " + prefix)
% plot(R(2:end), Vc(2:end), 'k--', 'DisplayName', "Correlation" )
% legend
% xlim([0.0, 10])
% % ylim([-5, 2])
% ylabel("$V($a.u$)$")
% xlabel("$r$")
% % set(gca, 'YScale', 'log');
% set(gca, 'XScale', 'log');
% exportf(f4, 'plots/Ne_cor.png')