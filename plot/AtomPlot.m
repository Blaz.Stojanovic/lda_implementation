clear all;
close all;

alfa = 27.211386245988;

% run("data/Kr_energies.m")
% run("data/Ar_energies.m")
% run("data/Ne_energies.m")
run("data/He_energies.m")
% prefix = "Kr";
% prefix = "Ne";
prefix = "He";
% prefix = "Ar";
suffix = ".txt";

% load data
R = load("data/" + prefix + "_R" + suffix);

Vh = load("data/" + prefix + "_Vh" + suffix);
Vext = load("data/" + prefix + "_Vext" + suffix);
Ve = load("data/" + prefix + "_Ve" + suffix);
Vc = load("data/" + prefix + "_Vc" + suffix);
Vks = load("data/" + prefix + "_Vks" + suffix);

% states
states = load("data/" + prefix + "_states" + suffix);
rho = load("data/" + prefix + "_rho" + suffix);
deg = load("data/" + prefix + "_deg" + suffix);
% deg = [2, 2, 6];

% plot individual states
names = ["1s", "2s", "2p", "3s", "3p", "4s", "3d", "4p", "5s", "4d", "5p", "6s", "4f"];

f3 = setfig('b3', [2*640 640]);
ymax = 2;
xmax = 5;
p = plot(R, 4*pi*R.^2.*rho, 'r-')
pref = 4*pi*R.^2;
for i=1:length(states(:, 1))
% for i=1:8
    [M,I] = max(deg(i)*pref.*states(i, :)');  %get max and index to place label
    plot(R, deg(i)*pref.*states(i,:)', 'k--')
    text(R(I) - xmax/1000, M + ymax/100, names(i))
end
legend([p(1)],"$n(r)$")
title("Radial electron density, " + prefix)
xlim([0.001, xmax])
ylim([0, ymax])
ylabel("$4 \pi r^2 n(r)$")
xlabel("$r$")
text(xmax*0.001, ymax*0.3, "E="+num2str(Etot*alfa) + "$eV$")

set(gca, 'XScale', 'log');
exportf(f3, 'plots/He_states.png')
