'''
Numerov algorithm for solving second order linear ODE of the form
x''(t) = f(t)x(t) + u(t)
because of the structure of the ODE, this algorithm leads to sixth order precision, using only second order integration
scheme.

~ https://en.wikipedia.org/wiki/Numerov%27s_method
'''
import numpy as np
import matplotlib.pyplot as plt

def numerov(f, _iter, s0, s1, dx):
    """
    Because (Usually) for Schroedinger equation u(t)=0, the following code solves x'' = fx

    :param f: function values at individual steps
    :param _iter: number of steps
    :param r0: starting integration point
    :param dx: step size
    :param s0: first bc
    :param s1: second bc
    :return: solution
    """
    h2 = dx**2 # step size square, to be used in new variable w
    h12 = h2/12
    solution = np.zeros(_iter)
    solution[0] = s0
    solution[1] = s1

    # first step
    w0 = (1-h12*f[0])*solution[0]
    fx = f[1]
    w1 = (1-h12*fx)*solution[1]
    X = solution[1]

    # time stepping
    for i in range(2, _iter):
        w2 = 2*w1 - w0 + h2*X*fx # timestep for w
        w0 = w1
        w1 = w2
        fx = f[i]
        X = w2/(1-h12*fx)
        solution[i] = X;

    # plt.plot(np.linspace(0, _iter*dx, _iter), solution)
    # plt.plot(np.linspace(0, _iter*dx, _iter + 1), f)
    # plt.show()

    return solution

def numerovFun(f, _iter, r0, s0, s1, dx):
    """
    Because (Usually) for Schroedinger equation u(t)=0, the following code solves x'' = fx

    :param f: function
    :param _iter: number of steps
    :param r0: starting integration point
    :param dx: step size
    :param s0: first bc
    :param s1: second bc
    :return: solution
    """
    h2 = dx**2 # step size square, to be used in new variable w
    h12 = h2/12
    solution = np.zeros(_iter)
    solution[0] = s0
    solution[1] = s1

    # first step
    w0 = (1-h12*f(r0))*solution[0]
    x = r0 + dx
    fx = f(x)
    w1 = (1-h12*fx)*solution[1]
    X = solution[1]

    # time stepping
    for i in range(2, _iter):
        w2 = 2*w1 - w0 + h2*X*fx # timestep for w
        w0 = w1
        w1 = w2
        x = x + dx
        fx = f(x)
        X = w2/(1-h12*fx)
        solution[i] = X;
    return solution

# quick test
# f = lambda x: -1
# sol = numerov(f, 100, 0, np.sin(0), np.sin(0.1), 0.1)
# plt.plot(np.linspace(0, 100*0.1, len(sol)), sol)
# plt.plot(np.linspace(0, 100*0.1, len(sol)), np.sin(np.linspace(0, 100*0.1, len(sol))))
# plt.show()

def numerovPoiss(rhs, _iter, s0, s1, dx):
    h2 = dx**2 # step size square, to be used in new variable w
    h12 = h2/12
    solution = np.zeros(_iter)
    solution[0] = s0
    solution[1] = s1

    # first step
    w0 = solution[0]-h12*rhs[0]
    rho_x = rhs[1]
    w1 = solution[1]-h12*rho_x
    X = solution[1]

    # iterate
    for i in range(2, _iter):
        w2 = 2*w1-w0+h2*rho_x
        w0 = w1
        w1 = w2
        rho_x = rhs[i]
        X = w2+h12*rho_x
        solution[i] = X

    return solution


"""quick test"""
# n = 1000
# x = np.linspace(0, 10 , n)
# rhs = -np.ones(n)
# sol = numerov(rhs, n, 0, x[1] - x[0], x[1] - x[0])
#
# plt.plot(x, np.sin(x))
# plt.plot(x, sol)
# plt.show()

# rhs = -np.
