"""
Main file for atom electron density and ground energy calculation
"""

import numpy as np
import hydrogen as hy
import numerov
import VWN_func as vwn
import QuantumState as qs
from scipy.optimize import brentq
from scipy.integrate import simps

eps_r = 1e-9 # radius offset, to avoid zero division

"""
Class for solving radially symmetric problems
"""
class RadialSolution:
    """
    Misc fields:
    Z - atomic number, this code does not work for ions. It assumes no. electrons = atomic number
    N - number of points in meshes
    Rmax - max radius
    rspace - radial mesh
    iteration - iteration

    ENERGIES: (at iteration)
    Eh - Hartree energy
    Epotential - KS potential energy
    Ekin - Kinetic energy
    Eeig - Bound state eigenvalues sum
    Exc - Exchange correlation energy
    Enucl - Interaction with nucleus
    Ecoul - Coulomb energy

    Etot0 - Total energy from previous iteration
    Etot - Total energy

    POTENTIALS: (at iteration)
    Vext - External potential
    Vhartree - Hartree potential
    Vx - Exchange potential
    Vcorr - Correlaiton potential
    VKS - Kohn-Sham potential (sum of all the above)


    states - n,l states up to principal number n_principal
    rho - electron density
    rho0 - electron density from previous iteration
    """

    def __init__(self, N, Rmax, Z):
        # Set params
        self.Z = Z
        self.N = N
        self.Rmax = Rmax
        self.iteration = 0
        self.Etot0 = 1000

        # Correlation and Exchange computation
        self.excor = vwn.VWN()
        # small value added to resolve div 0 issues at origin
        self.R = np.linspace(0.0, self.Rmax, self.N) + eps_r
        self.dr = self.R[1] - self.R[0] # step in calculations todo this wont be necessary anymore
        # set external potential, this does not change throughout the computation
        self.setExternal()
        # calculate number of principal states
        self.n_principal = hy.NoOP(Z)
        # construct states
        self.states = qs.ConstructStates(self.n_principal)
        # todo add logarithmic grid support

    """
    el density guess
    """
    def initializeDensity(self):
        self.rho0 = hy.density(self.R, self.Z) # initial density
        self.rho = hy.density(self.R, self.Z) # we need two densities for iteration

    """
    returns r_s for given rho
    Not the best name for function
    """
    def getr_s(self, rho):
        return np.power(3/(4*np.pi)*np.reciprocal(rho), 1/3.)

    """
    Solve radial poisson equation
    U''(r) = -4*pi*rho(r)
    and set Hartree potential
    """
    def setHartree(self, rho):
        # set rhs to -4*r*pi*el.density
        RHS = -4*np.pi*rho*self.R
        dr = self.R[1]-self.R[0]
        h0 = 0
        h1 = dr  # initial condition du/dr = 1, there may be a better way, but this is sufficient
        # integrate
        temp_hartree = numerov.numerovPoiss(RHS, len(RHS), h0, h1, dr)

        # make fit boundary conditions
        alpha = (self.Z - temp_hartree[-1])/self.Rmax
        self.Uhartree = (temp_hartree + alpha*self.R) # set Uhartree

        # fix potential: Uhartree = temp_hartree + r(Z - temp_hartree(Rmax))/Rmax
        # Vhartree = (temp_hartree[1:] + alpha*self.R[1:])*np.reciprocal(self.R[1:])
        # Vhartree = np.append(2*Vhartree[0] - Vhartree[1], Vhartree)  # for numerical stabilty

        # Vhartree = Uhartree/r
        Vhartree = self.Uhartree*np.reciprocal(self.R)
        self.Vhartree = Vhartree

    """
    Set external potential to Vext = -Z/r 
    this only needs to be done once, as this potential does not change
    """
    def setExternal(self):
        temp = -self.Z * np.reciprocal(self.R[1:]) # the value is not defined at r = 0
        self.Vext = np.append(0, temp)

    """
    Set Exchange potential Vex
    Using ExchangeCorrelation class
    """
    def setExchange(self, r_s):
        Vx = (self.excor).Vx(r_s)
        # numerical stability at zero
        # Vx[0] = 2*Vx[1] - Vx[2]
        self.Vx = Vx

    """
    Set Correlation potential Vcorr
    Using ExchangeCorrelation class
    """
    def setCorrelation(self, r_s):
        Vcorr = (self.excor).Vc(r_s)
        # numerical stability at zero
        # Vcorr[0] = 2 * Vcorr[1] - Vcorr[2]
        self.Vcorr = Vcorr

    """
    Set Kohn-Sham potential, as sum of all potentials
    """
    def setKS(self, rho):
        # calculate KS, Vks = Vext + Vh + Vx + Vc
        r_s = self.getr_s(rho)
        # calculate individual potentials. Take care, this computes Hartree potential as well!
        self.setHartree(rho)
        self.setExchange(r_s)
        self.setCorrelation(r_s)

        self.VKS = self.Vext + self.Vhartree + self.Vx + self.Vcorr

    """
    Get effective radial potential, that includes centrifugal term due to l
    """
    def getEffectivePotential(self, l, E):
        # Calculate effective radial potential, including centrifugal term
        # returns the effective potential, with quantum number l
        temp = 2 * (self.VKS[1:] + l * (l + 1) * np.reciprocal(2 * np.square(self.R[1:])) - E)
        at0 = temp[0]*2 - temp[1] # numerical stabiliy
        return np.append(at0, temp)

    def findBoundState(self, qs, Estep, Eprev, Emax=10, eps=1e-10):
        """
        finds bound state for given energy, by
        solving
        u''(r) = Veff(r)*u(r); u(0) = 0, u(inf) = 0

        :param QuantumState: Quantum state object
        :param Emax: Max energy
        :param Estep: energy steps made when looking for eigenvalues
        :param Eprev: energy of the previous calculated eigenvalue, for speedup purposes
        :return: None
        """

        # get l
        l = qs.l

        # get n
        n = qs.n

        # IMPORTANT we will be integrating Schrodinger's equation from the outer edge towards the center. This is more
        # stable, due to the oscillations of the solutions near the origin. This means the solution vector is reversed.

        # set low bound for energy search
        # to save time we assume that E(n, l) < E(n, l + 1)
        if (n == 0 and l == 0): # todo, this can be improved
            E = -0.5*self.Z*self.Z - 3.
        else:
            E = Eprev + 1e-7# previous energy
            # print("used previous energy of, " + str(Eprev))

        """
        Helper function for individual shot of the shooting method
        """
        # todo this is ugly maybe fix later
        def shoot(E, fullout=False):
            # print(E)
            # set initial conditions
            s0 = hy.Hu(self.Rmax, self.Z)
            s1 = hy.Hu(self.Rmax - self.dr, self.Z)

            # integrate for rhs obtained with E, Veff needs to be reversed because we are
            # integrating in the opposite direction
            rhs = self.getEffectivePotential(l, E)
            # addressing numerical instability due to 1/r = inf
            rhs[0] = rhs[1]*2 - rhs[2]
            rhs = np.flip(rhs)
            solution = numerov.numerov(rhs, self.N-1, s0, s1, self.dr)
            # extrapolate to zero
            solution = np.append(solution, solution[-1]*(2+self.dr*self.dr*rhs[-2])-solution[-2])

            if fullout:
                return solution
            # return the last value in the array
            return solution[-1]


        # calculate boundary values of solutions
        u0 = shoot(E)

        while(E <= Emax):
            # step forward with energy
            E = E + Estep
            # new bval
            u1 = shoot(E)

            # if change of sings, we have a root of the function
            if u1*u0 < 0:
                # search for zero
                E_nl = brentq(shoot, E - Estep, E, xtol=eps)
                # store eigenvalue and function
                qs.E = E_nl
                wavefun = np.flip(shoot(E_nl, True)) # flipped in the right direction

                density, wf = self.normalize(wavefun) # normalize
                qs.setDensity(density)
                qs.setWavefunc(wf)
                #end loop
                break

            # swap
            u0 = u1

    def normalize(self, wavefun):
        """
        Normalize wave funciton so that \int_0^{r_max} wavefun^2 = 1
        The radial electron density is wavefun^2/(4pi*r^2)
        :param wavefun: wave function
        :return: normalized density for the state
        """
        u2 = (wavefun*wavefun)[1:] # without first point, to avoid instabilities
        integral = simps(u2, self.R[1:])
        rho = u2/(4*integral*np.pi)*np.square(np.reciprocal(self.R[1:]))
        return np.append(2*rho[0] - rho[1], rho), wavefun/integral # extrapolated value at 0

    """
    Finds all bound states, sort them by ascending energy
    """
    def findBoundStates(self, Estep, Emax=10, eps=1e-10):
        # iterate over all of the states of the system
        Eprev = None # ugly fix, but will do
        for state in self.states:
            self.findBoundState(state, Estep, Eprev)
            Eprev = state.E # temp energy store

        # sort states by energy, smallest to largest
        self.states = sorted(self.states)

    """
    New density from orbitals
    """
    def getNewDensity(self):
        Nadd = 0 # number of added electrons
        Esum = 0 # sum of eigenvalues

        # init new density that will be constructed from n,l states
        rho_con = np.zeros(self.N)

        # start adding orbitals
        for state in self.states:
            # print(state)
            l = state.l
            degeneracy = 2*(2*l + 1)

            # be careful if the atom is almost full/the shell is not full
            if Nadd + degeneracy < self.Z: # if there is "space" for electrons
                fermi = 1.
            else: # Nadd + degenerace >= Z
                # only add a fraction of possible el.
                fermi = (self.Z - Nadd)/(2*(2*l + 1))

            # add to density
            rho_con = rho_con + state.rho_nl*degeneracy*fermi
            # sum of energies
            Esum = Esum + state.E*degeneracy*fermi
            # count added electrons
            Nadd = Nadd + degeneracy

            # store degeneracy of each state
            state.setDegeneracy(degeneracy*fermi)

            # output
            print("Added state, n = ", state.n, ", l = ", state.l, ", E = ", state.E,
                  ", No. el. states = ", degeneracy*fermi, ", filled states ", Nadd, "/", self.Z)

            # break the loop when full
            if (Nadd >= self.Z):
                break

        return rho_con, Esum

    """
    Set new electron density 
    """
    def setNewDensity(self):
        rho, E = self.getNewDensity()
        self.Eeig = E # set eigenvalue energy aswell
        self.rho = rho

    """
    Calculate total energy
    """
    def calculateEnergy(self, rho0, rho):
        R2 = np.square(self.R) # square of distances
        # Hartree contribution int -0.5*4*n*Vh*r^2
        Eh = -2*np.pi*simps(self.rho*self.Uhartree*self.R, self.R)

        # Exchange correlation contribution
        r_s = self.getr_s(rho0)
        temp = 4*np.pi*((self.excor).ExVx(r_s) + (self.excor).EcVc(r_s))*self.rho*R2
        Exc = simps(temp, self.R)

        # nuclei interaction
        Enucl = -4*np.pi*self.Z*simps(self.rho0*self.R, self.R)

        # potential Vks
        Epotential = 4*np.pi*simps(R2*self.VKS*self.rho0, self.R)

        # store the energies
        self.Eh = Eh
        self.Exc = Exc
        self.Enucl = Enucl
        self.Epotential = Epotential

        self.Etot = self.Eeig + Eh + Exc
        self.Ekin = self.Eeig - Epotential
        self.Ecoul = Enucl - Eh


    """
    Construct new electron density, using Linear mixing
    """
    def updateDensityLM(self, mixing):
        temp = (1 - mixing)*self.rho0 + mixing*self.rho
        self.rho0 = temp  # swap
                          # rho will be set when constructing new density in the next iteraiton

    """
    Construct new electron density, using Broyden mixing
    """
    def updateDensityBR(self): # todo
        pass

    """
    Self consistency loop
    """
    def solve(self, MaxIter, mixing_coef, Etol=1e-8):
        self.iteration = 0
        # guess initial density
        self.initializeDensity()


        # iterate
        while(self.iteration < MaxIter):
            # set KS, from previous denisty
            self.setKS(self.rho0)  # also sets Vh, Vex, Vcorr

            # Calculate how electrons can be bound in this potential
            self.findBoundStates(1e-1)

            # Construct new density from above bound states & Eeig
            self.setNewDensity() # this updates rho!

            # Calculate energies
            self.calculateEnergy(self.rho0, self.rho)

            # iteration done
            self.iteration += 1

            self.output()
            # check self consistency
            if (abs(self.Etot0 - self.Etot)/abs(self.Etot)) < Etol:
                print("Self consistency loop terminated at E_tot = ", self.Etot)
                break
            else:
                self.Etot0 = self.Etot
                self.updateDensityLM(mixing_coef) # this sets rho0 to the newly combined density


    def output(self):

        # output information about the state of the solution
        print("")
        print("After iteration: ", self.iteration)
        print("Total energy: ", self.Etot)
        print("Kinetic energy: ", self.Ekin)
        print("Hartree energy: ", self.Eh)
        print("ExCor Energy: ", self.Exc)
        print("Potential energy: ", self.Epotential)
        print("Nucleus interaction Energy: ", self.Enucl)
        print("Coulomb energy: ", self.Ecoul)
        print("Eigen energy: ", self.Eeig)
        print("------------------------------------------------------------------------------")


    def saveToFile(self, filename, suffix=".txt"):
        # todo, fix this shit
        # save radial mesh
        np.savetxt(filename + "_R" + suffix, self.R, delimiter=',')

        # save potentials
        np.savetxt(filename + "_Vh" + suffix, self.Vhartree, delimiter=',')
        np.savetxt(filename + "_Vext" + suffix, self.Vext, delimiter=',')
        np.savetxt(filename + "_Ve" + suffix, self.Vx, delimiter=',')
        np.savetxt(filename + "_Vc" + suffix, self.Vcorr, delimiter=',')
        np.savetxt(filename + "_Vks" + suffix, self.VKS, delimiter=',')

        # save energies
        en_file =  open(filename + "_energies.m", 'w')
        en_file.write("Etot="+str(self.Etot)+";\n")
        en_file.write("Ekin="+str(self.Ekin)+";\n")
        en_file.write("Ehar="+str(self.Eh)+";\n")
        en_file.write("Eec="+str(self.Exc)+";\n")
        en_file.write("Epot="+str(self.Epotential)+";\n")
        en_file.write("Enuc="+str(self.Enucl)+";\n")
        en_file.write("Ecou="+str(self.Ecoul)+";\n")
        en_file.write("Eeig="+str(self.Eeig)+";\n")

        # radial electron density
        np.savetxt(filename + "_rho" + suffix, self.rho, delimiter=',')

        # all individual states, and degeneracies
        deg = np.zeros(len(self.states))
        energies = np.zeros(len(self.states))
        stat = np.zeros((len(self.states), len((self.states[0]).rho_nl)))
        wave = np.zeros((len(self.states), len((self.states[0]).wave_nl)))
        for i, state in enumerate(self.states):
            stat[i] = state.rho_nl
            deg[i] = state.deg
            wave[i] = state.wave_nl
            energies[i] = state.E
        np.savetxt(filename + "_states" + suffix, stat, delimiter=',')
        np.savetxt(filename + "_wave" + suffix, wave, delimiter=',')
        np.savetxt(filename + "_deg" + suffix, deg, delimiter=',')
        np.savetxt(filename + "_energies" + suffix, energies, delimiter=',')


# ------------------------------------------------------------------------------------------------------------------- #


"""Testing"""
if __name__ == '__main__':
    precision = 1e-8
    rmax = 10
    dr = 0.001
    mix = 0.3
    Estep = 0.2
    N = int((rmax/dr + 1.5) // 1)

    # He = RadialSolution(N, rmax, 2)
    # He.solve(100, mix, Etol=precision)
    # He.saveToFile("../plot/data/He")

    # Li = RadialSolution(N, rmax, 3)
    # Li.solve(100, mix, Etol=precision)
    # Li.saveToFile("../plot/data/Li")

    # Be = RadialSolution(N, rmax, 4)
    # Be.solve(100, mix, Etol=precision)
    # Be.saveToFile("../plot/data/Be")

    # B = RadialSolution(N, rmax, 5)
    # B.solve(100, mix, Etol=precision)
    # B.saveToFile("../plot/data/B")

    # C = RadialSolution(N, rmax, 6)
    # C.solve(100, mix, Etol=precision)
    # C.saveToFile("../plot/data/C")

    # Nitrogen = RadialSolution(N, rmax, 7)
    # Nitrogen.solve(100, mix, Etol=precision)
    # Nitrogen.saveToFile("../plot/data/N")

    # O = RadialSolution(N, rmax, 8)
    # O.solve(100, mix, Etol=precision)
    # O.saveToFile("../plot/data/O")

    # F = RadialSolution(N, rmax, 9)
    # F.solve(100, mix, Etol=precision)
    # F.saveToFile("../plot/data/F")

    # Ne = RadialSolution(N, rmax, 10)
    # Ne.solve(100, mix, Etol=precision)
    # Ne.saveToFile("../plot/data/Ne")

    # Na = RadialSolution(N, rmax, 11)
    # Na.solve(100, mix, Etol=precision)
    # Na.saveToFile("../plot/data/Na")

    # Mg = RadialSolution(N, rmax, 12)
    # Mg.solve(100, mix, Etol=precision)
    # Mg.saveToFile("../plot/data/Mg")

    # Al = RadialSolution(N, rmax, 13)
    # Al.solve(100, mix, Etol=precision)
    # Al.saveToFile("../plot/data/Al")

    # Si = RadialSolution(N, rmax, 14)
    # Si.solve(100, mix, Etol=precision)
    # Si.saveToFile("../plot/data/Si")

    # P = RadialSolution(N, rmax, 15)
    # P.solve(100, mix, Etol=precision)
    # P.saveToFile("../plot/data/P")

    # S = RadialSolution(N, rmax, 16)
    # S.solve(100, mix, Etol=precision)
    # S.saveToFile("../plot/data/S")

    # Cl = RadialSolution(N, rmax, 17)
    # Cl.solve(100, mix, Etol=precision)
    # Cl.saveToFile("../plot/data/Cl")

    Ar = RadialSolution(N, rmax, 18)
    Ar.solve(100, mix, Etol=precision)
    Ar.saveToFile("../plot/data/Ar")

    # Kr = RadialSolution(N, rmax, 36)
    # Kr.solve(100, mix, Etol=precision)
    # Kr.saveToFile("../plot/data/Kr")
