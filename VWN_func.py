"""
Implementation of functional - S.H.Vosko, L.Wilk, and M.Nusair, Can.J.Phys.58, 1200 (1980)
"""
import numpy as np

class VWN():
    def __init__(self):
        self.alphax = 0.610887057710857  # //(3/(2 Pi))^(2/3)
        self.Aw = 0.0311
        self.Bw = -0.048
        self.Cw = 0.002
        self.D = -0.0116
        self.gamma = -0.1423
        self.beta1 = 1.0529
        self.beta2 = 0.3334
        self.Ap = 0.0621814
        self.xp0 = -0.10498
        self.bp = 3.72744
        self.cp = 12.9352
        self.Qp = 6.1519908
        self.cp1 = 1.2117833
        self.cp2 = 1.1435257
        self.cp3 = -0.031167608

    def Vx(self, rs):  # Vx
        return -self.alphax*np.reciprocal(rs)


    def ExVx(self, rs):  # Ex-Vx
        return 0.25*self.alphax*np.reciprocal(rs)


    def Ex(self, rs):
        return -0.75*self.alphax/rs

    def Vc(self, rs):  # Vc
        x=np.sqrt(rs)
        xpx=x*x+self.bp*x+self.cp
        atnp=np.arctan(self.Qp * np.reciprocal(2 * x + self.bp))
        ecp=0.5*self.Ap*(np.log(x*x/xpx)+self.cp1*atnp-self.cp3*(np.log((x-self.xp0)**2*np.reciprocal(xpx))+self.cp2*atnp))

        return ecp-self.Ap/6.*(self.cp*(x-self.xp0)-self.bp*x*self.xp0)*np.reciprocal((x-self.xp0)*xpx)

    def EcVc(self, rs):  # Ec-Vc
        x = np.sqrt(rs)
        return self.Ap/6.*(self.cp*(x-self.xp0)-self.bp*x*self.xp0)*np.reciprocal((x-self.xp0)*(x*x+self.bp*x+self.cp))

